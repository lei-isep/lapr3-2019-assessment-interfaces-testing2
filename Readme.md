# README #

This is the repository used for holding the interfaces to be used in the LAPR3 projet.

#Java source files

Java source and test files are located in folder src.

# Maven files #

Pom.xml file controls the project build.

# Eclipse files #

The following files are solely used by Eclipse IDE:

* .classpath
* .project

# IntelliJ Idea IDE files #

The following folder is solely used by Intellij Idea IDE :

* .idea

## How was the .gitignore file generated? ##
.gitignore file was generated based on https://www.gitignore.io/ with the following keywords:
  - Java
  - Maven
  - Eclipse
  - NetBeans
  - Intellij

## Who do I talk to? ##
In case you have any problem, please email Nuno Bettencourt (nmb@isep.ipp.pt).

## How do I use Maven? ##

To generate the jar file to be included in your project, execute the "package" goal.

`$ mvn clean test package`

### How to generate the javadoc for source code? ###
Execute the "javadoc:javadoc" goal.

`$ mvn javadoc:javadoc`

This generates the source code javadoc in folder "target/site/apidocs/index.html".

# How to add the interfaces to your project #

## Deploy the jar file to your project ##
Deploy the generated bike-sharing-assessment-interfaces-X.X.jar (where X.X represents the version number) to your project third-parties folder.

## Changes to POM.XML ##
Add the following lines to your project pom.xml file, in the dependencies tag. 

Don't forget to change the version number.   

```xml
<dependency>
    <groupId>lapr3</groupId>
    <artifactId>bike-sharing-assessment-interfaces</artifactId>
    <version>0.1</version>
    <scope>system</scope>
    <systemPath>${project.basedir}/third-parties/bike-sharing-assessment-interfaces-X.X.jar</systemPath>
</dependency>
```

## Add new code to implement the Serviceable Class ##
Create a Facade class, in the lapr.project.assessment, that implements the Serviceable interface.

## Update .gitignore file ##
Add the following lines to your project's .gitignore file, so that it can manage these jars.
```
#Include the interfaces jar, so that your project continues to compile.
!third-parties/bike-sharing-assessment-interfaces*.jar
```